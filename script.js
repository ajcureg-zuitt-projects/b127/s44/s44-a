const getData = () => {
	fetch('https://jsonplaceholder.typicode.com/users')
	.then(response => {
		return response.json();
	}).then(data => {
		let users = "";
		data.forEach(user => {
			users += `
			<div>
				<div class="card-group">
					<div class="card">
						<img src="images.jpg" class="card-img-top-thumbnail" alt="...">
						<div class="card-body">
							<h3>Name: ${user.name}</h3>
							<p>Email: ${user.email}</p>
							<p>Company: ${user.company}</p>
							<p>Phone: ${user.phone}</p>
						</div>
					</div>
				</div>
			</div>
			`
		});
		document.querySelector('#output').innerHTML = users;
	})
	.catch(err => {
		console.log(err)
	});
}


const btn1 = document.querySelector('#btn1');
btn1.addEventListener('click', getData)